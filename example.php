<?php

require_once 'vendor/autoload.php';

use Planet17\PushNotifications\Mailing\PushManager;
use Planet17\PushNotifications\Mailing\Transmitters\{FCM as FCMTransmitter, APNS as APNSTransmitter};
use Planet17\PushNotifications\Pushes\Receivers\FCMReceiversCollection;
use Planet17\PushNotifications\Contracts\Pushes\{
    Services\BasePushContract,
    Services\FCMPushContract as FCMPush,
    Platform\IOS\PushSubtitledContract,
    Platform\IOS\PushCategorizedContract
};
use Planet17\PushNotifications\Pushes\Specific\FCM\{ActionsBundle, Action};
use Planet17\PushNotifications\Common\Service;
use Planet17\PushNotifications\Mailing\Response\FCM as FCMResponse;
use Planet17\PushNotifications\Pushes\Payloads\FCM;
use Example\Resolvers\Channels\{Map as ChannelMap, Resolver as ChannelResolver, Rule as ChannelRule};
use Example\Resolvers\Payloads\{Map as TypesMap, Resolver as TypesResolver, Rule as TypesRule};


$repositoriesDir     = dirname(__DIR__) . '/resources/Autodoc/Repositories';

$exampleOfContent = [
    'title' => 'title',
    /* fill other props if need to run it ... */
];

$fcmAccounts = [
    'acc1' => ['name' => 'acc1', 'key' => 'exaorpwoperwri'],
    'acc2' => ['name' => 'acc2', 'key' => 'exaorpwoperwri'],
    'acc3' => ['name' => 'acc3', 'key' => 'exaorpwoperwri'],
    'acc4' => ['name' => 'acc4', 'key' => 'exaorpwoperwri'],
    'acc5' => ['name' => 'acc5', 'key' => 'exaorpwoperwri'],
];

$instances = [
    1 => [
        'env' => 'dev',
        'platform' => 'IOS',
        'project' => 'projectE',
        'language' => 'ch'
    ]
];


$channelResolver = new ChannelResolver(new ChannelMap(new ChannelRule()));
$payloadsTypesResolver = new TypesResolver(new TypesMap(new TypesRule()));

$pushManager = new PushManager();
foreach ($fcmAccounts as $account) {
    switch ($account['service']) {
        case 'fcm':
            $pushManager->setChannel($account['name'], new FCMTransmitter($account['key']));
            break;
        case 'apns':
            $pushManager->setChannel(
                $account['name'],
                new APNSTransmitter($account['certificates']['production'], false)
            );
            break;
        default:
            throw new \InvalidArgumentException('Unresolved service type');
    }
}


$contentType = 101;

$groupedInstances = [];

foreach ($instances as $instance) {
    $language = $instance['language'];

    $condition        = [
        //ChannelRule::OPT_NAME_PLATFORM    => $platformMapper->getPlatformName($instance[ ChannelRule::OPT_NAME_PLATFORM ]),
        ChannelRule::OPT_NAME_ENVIRONMENT => $instance['env'],
    ];

    $payloadTypeClass = $payloadsTypesResolver->resolve($condition);

    $condition[ChannelRule::OPT_NAME_PROJECT] = $instance[ ChannelRule::OPT_NAME_PROJECT ];
    $channel     = $channelResolver->resolve($condition);

    $groupedInstances[ $payloadTypeClass ][ $contentType][ $language ][ $channel ][] = $instance;
}

foreach ($groupedInstances as $payloadTypeClass => $groupByPayload) {
    /** @var BasePushContract $prototype */
    $prototype = new $payloadTypeClass;
    foreach ($groupByPayload as $contentType => $groupByContent) {
        foreach ($groupByContent as $language => $groupByLanguage) {
            foreach ($groupByLanguage as $channel => $groupByChannel) {
                /* get by language */

                $push = clone $prototype;

                $push->setTitle($exampleOfContent['subtitle']);
                $push->setBody('Mailing Full Cycle Test');
                if ($push instanceof FCMPush) {
                    /** @var FCMPush|FCM $push */
                    $push->setIcon($exampleOfContent['icon']);
                    $push->setClickAction($exampleOfContent['links']['default']);
                    $push->setTimeToLive(15);
                    $push->setImage($exampleOfContent['image']);
                    $push->setTag($exampleOfContent['tag']);
                }

                if ($push instanceof PushSubtitledContract) {
                    /** @var PushSubtitledContract $push */
                    $push->setSubtitle($exampleOfContent['subtitle']);
                }

                if ($push instanceof PushCategorizedContract) {
                    /** @var PushCategorizedContract $push */
                    $push->setCategory($exampleOfContent['tag']);
                }

                $actions = new ActionsBundle();
                foreach ($exampleOfContent['actions'] as $action) {
                    $actions->pushAction(new Action($action['action'], $action['title'], $action['icon']));
                }

                $push->setActions($actions);

                foreach (array_chunk($groupByChannel, Service::LIMIT_RECEIVERS_PER_REQUEST[ Service::FCM ]) as $chunk) {
                    $cloned     = clone $push;
                    $collection = new FCMReceiversCollection();
                    $instances  = array_map(function($i) { return $i['token']; }, $chunk);
                    $collection->setTokens($instances);
                    $cloned->setReceivers($collection);
                    /** @var FCMResponse[] $responses */
                    $responses = $pushManager->send($channel, $cloned);

                    $exampleOfContent = 'Push Notification had been sent to ' . \count($instances)
                                        . ' instances. ' . PHP_EOL . ' (account:"' . $channel
                                        . '", via Payload\'s Class: "' . $payloadTypeClass
                                        . '", at language:"' . $language . '")' . PHP_EOL;
                    $exampleOfContent .= $responses[0]->getSuccessCount() . ' - SUCCESS.' . PHP_EOL;
                    $exampleOfContent .= $responses[0]->getFailureCount() . ' - FAIL.' . PHP_EOL . PHP_EOL;
                    echo $exampleOfContent;
                }
            }
        }
    }
}
