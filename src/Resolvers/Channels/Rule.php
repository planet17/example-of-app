<?php


namespace Example\Resolvers\Channels;


use Planet17\RulesMapResolver\Contracts\RuleContract;


class Rule extends \Planet17\PushNotifications\ChannelsRoutingResolver\Rule
{
    /** @inheritdoc */
    public function getOptsNames():array
    {
        return [
            self::OPT_NAME_PLATFORM,
            self::OPT_NAME_ENVIRONMENT,
            self::OPT_NAME_PROJECT
        ];
    }


    /**
     * Current project group name from where receivers come.
     *
     * @const OPT_NAME_PROJECT
     */
    const OPT_NAME_PROJECT = 'project';


    /**
     * Method more secure append one of `Option` values.
     *
     * @param string $value
     *
     * @return RuleContract|Rule
     */
    public function addProject(string $value):RuleContract
    {
        return $this->addOptValue(self::OPT_NAME_PROJECT, $value);
    }
}

