<?php


namespace Example\Resolvers\Channels;


use Planet17\PushNotifications\Common\Platforms;


class Map extends \Planet17\PushNotifications\ChannelsRoutingResolver\Map
{
    public function setUp()
    {
        $this->addRule(
            'acc1',
            $this->getPrototype()
                 ->addPlatform(Platforms::PLATFORM_BROWSER_DESKTOP)
                 ->addPlatform(Platforms::PLATFORM_BROWSER_MOBILE)
        );

        $this->addRule(
            'acc2',
            $this->getPrototype()
                 ->addPlatform(Platforms::PLATFORM_APPLICATION_IOS)
                 ->addEnvironment('test')
        );

        $this->addRule(
            'acc3',
            $this->getPrototype()
                 ->addPlatform(Platforms::PLATFORM_APPLICATION_ANDROID)
                 ->addPlatform(Platforms::PLATFORM_APPLICATION_IOS)
                 ->addEnvironment('production')
        );

        $this->addRule(
            'acc4',
            $this->getPrototype()
                 ->addPlatform(Platforms::PLATFORM_APPLICATION_ANDROID)
                 ->addEnvironment('production')
        );

        $this->addRule(
            'acc5',
            $this->getPrototype()
                 ->addPlatform(Platforms::PLATFORM_APPLICATION_ANDROID)
                 ->addEnvironment('test')
        );
    }
}

