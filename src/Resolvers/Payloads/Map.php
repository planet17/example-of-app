<?php


namespace Example\Resolvers\Payloads;


use Planet17\PushNotifications\Common\Platforms;
use Example\PayloadsStructureTypes\Extended;
use Example\PayloadsStructureTypes\IOS;
use Example\PayloadsStructureTypes\Simple;


class Map extends \Planet17\PushNotifications\PayloadsStructureTypesMapperResolver\Map
{
    public function setUp()
    {
        $this->addRule(
            Extended::class,
            $this->getPrototype()
                 ->addPlatform(Platforms::PLATFORM_BROWSER_DESKTOP)
                 ->addPlatform(Platforms::PLATFORM_BROWSER_MOBILE)
        );

        $this->addRule(
            IOS::class,
            $this->getPrototype()
                 ->addPlatform(Platforms::PLATFORM_APPLICATION_IOS)
        );

        $this->addRule(
            Simple::class,
            $this->getPrototype()
                 ->addPlatform(Platforms::PLATFORM_APPLICATION_ANDROID)
        );
    }
}
